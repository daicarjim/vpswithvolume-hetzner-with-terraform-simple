# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "hcloud_token" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

# Create a server
resource "hcloud_server" "web" {
  name        = "rousse-server"
  image       = "ubuntu-18.04"
  server_type = "cx11"
}

# Create a volume
resource "hcloud_volume" "storage" {
  name       = "rousse-volume"
  size       = 50
  server_id  = hcloud_server.web.id
  automount  = true
  format     = "ext4"
}

